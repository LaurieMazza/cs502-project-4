Author:	             Stephen Crawford (scrawfordv3.0@gmail.com; slcrawford@wpi.edu)
                            Laurie Mazza (lmazza@wpi.edu)
                            Project Group 1-6
Date:	                    03 March 2022
Version:                    0.9
Project ID:	             Project 4 - Memory Allocation
CS Class:                   CS502 - Operating Systems
Programming Language:		C
OS/Hardware dependencies:	Ubuntu 20.04 LTS
				
Problem Description:		

Interfaces:			
	User                    Linux CLI

How to build the program:	Change directory to the assigned-project-processes directory
                            gcc -o wshell wshell.c -Wall -Werror

Program Source:			    goatmalloc.c


Results:			        All utilities pass required test scripts

Test Procedures:		    Use provided test scripts
Test Data:			        Provided by professor
