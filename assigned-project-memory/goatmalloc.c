#include <stddef.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/mman.h>

#include "goatmalloc.h" //this should be the last include

void *_arena_start; //point to start of arena
struct __node_t *list; 
int statusno;
int arena_size;

//init(size_t size) function used to initialize memory allocator
int init(size_t size){
	arena_size = size;
	size_t psize = getpagesize();
	
	//check if size is valid; swap to error code/setting error var
	if( arena_size <= 0){
		statusno = -2;
		return statusno;
	}
	
	//calculate arena size
	//check if it's a multiple of page size, if not, make it one
	while(!(arena_size % psize == 0)){
		//increase the area size to the next closest multiple
		arena_size = arena_size + (psize - (arena_size % psize));
	} 
	
	//check if these enough memory
	if(arena_size < (size + sizeof(node_t))){
		statusno = -1;
		//return arena_size;
	}
	
	_arena_start = mmap(NULL, arena_size, PROT_READ | PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, 0);
	
	//init chunk list
	list = _arena_start; 
	list->size = arena_size - sizeof(node_t);
	list->is_free = 0;
	list->fwd = NULL;
	list->bwd = NULL;
	
	return arena_size; 
}

//walloc(size_t size) function called by application code to request new memory allocation of size bytes
void* walloc(size_t size){
	node_t *cur;
	void *result;
	
	//check if area exists?
	if(!(_arena_start)){
		statusno = -5;
		result = NULL;
		return result;
	}

	//track location in arena
	cur = list;
	
	//check if these enough memory
	if(arena_size < (size + sizeof(node_t))){
		statusno = -1;
		result = NULL;
		return result;
	}
	
	//adjust size for header?
	size_t actSize = size + sizeof(node_t);
	
	//move through to find region that is equal to or larger than the size
	while(((cur->size)<size) | (((cur->is_free)==0) && ((cur->fwd)!=NULL))){
		cur=cur->fwd;
	}
	
	//check if these enough memory
	if((cur->size) < size){
		statusno = -1;
		result = NULL;
		return result;
	}
	
	//if equal, set and return
	if(cur->size == size ){
		cur->is_free = 0;
		result = (void*)(++cur);
		return result;
	}
	
	//if larger found, split based on needed size
	else if ((cur->size)>actSize){
	//new chunk
		node_t *new = (void*)((void*)cur+size+sizeof(node_t));
		new->size = (cur->size) - actSize;
		new->is_free = 1;
		new->fwd = cur->fwd;
		new->bwd = cur;
		
	//set old chunk
		cur->size = size;
		cur->is_free = 0;
		cur->fwd = new;
		
	//results
		result = (void*)(++cur);
		return result;
	}
	
	//if no region is found, set statusno and return null
	else{
		statusno = -1;
		result = NULL;
		return result;
	}

}

//wfree(void *ptr) function called by app code to free up existing memory chunks
void wfree(void *ptr){
	node_t *cur, *clist;
	if((_arena_start <= ptr) && (ptr <= (void*)(_arena_start+arena_size))){
		cur = ptr;
		--cur;
		cur->is_free = 1;

		clist=list;
		while((clist->fwd) != NULL){
			if((clist->is_free) && (clist->fwd->is_free)){
				clist->size += (clist->fwd->size) + sizeof(node_t);
				clist->fwd = clist->fwd->fwd;
			}
			clist = clist->fwd;
		}
	}
	else{
		statusno = -2;//??
	}
}

//destroy() function returns the arena's memory to the OS and reset all internal state variables
int destroy(){
	//check if initialized before, return error if not
	if(arena_size == -1){
		statusno = -5;
		return statusno;
	}
	
	int err = munmap(_arena_start, arena_size);
	
	//reset internal tracking
	_arena_start = NULL;
	list = NULL;
	arena_size = 0;
	
	return err; 
}


